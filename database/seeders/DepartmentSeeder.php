<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('departments')->insert([
            'title' => 'HR',
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('departments')->insert([
            'title' => 'IT',
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('departments')->insert([
            'title' => 'Finance',
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
