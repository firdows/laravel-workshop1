<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'Amin',
            'email' => 'admin@localhost.com',
            'password' => Hash::make('123456789'),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
