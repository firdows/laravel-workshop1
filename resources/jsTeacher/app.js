import { createApp } from "vue";

import "./bootstrap";

import App from "./App.vue";
import Router from "./routers/index";//index.js

const app = createApp(App);
app.use(Router);
app.mount("#app");
