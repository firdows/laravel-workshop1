import { createRouter, createWebHistory } from "vue-router";
import { h, resolveComponent } from "vue";

import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import DashBoard from "../layouts/DashBoard.vue";
import DepartmentIndex from "../views/department/DepartmentIndex.vue";

export const routes = [
    {
        name: "DashBoard",
        path: "/",
        component: DashBoard,
        children: [
            {
                name: "Home",
                path: "",
                component: Home
            },
            {
                name: "Department",
                path: "department",
                component: {
                    render: () => h(resolveComponent("router-view"))
                },
                children: [
                    {
                        name: "DepartmentIndex",
                        path: "",
                        component: DepartmentIndex
                    },
                ]
            },
        ]
    },
    {
        name: "Login",
        path: "/login",
        component: Login,
    },
];

const router = createRouter({
    routes: routes,
    history: createWebHistory("/laravel8ssup/public/"),
    linkExactActiveClass: "active"
});

export default router;
