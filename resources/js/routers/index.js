import { createRouter, createWebHistory } from "vue-router";

import { h, resolveComponent } from "vue";

import departmentRoutes from "../views/departments/department-route";
import errorRoutes from "../views/error/error-route";
import Page404 from "../views/error/page404.vue";

//import NotFound from "./components/404.vue";

import DashBoard from "../layouts/DashBoard.vue";
import Home from "../views/Home.vue";
import About from "../views/About.vue";
import Contact from "../views/Contact.vue";
import Login from "../views/Login.vue";
import Sale from "../views/sale/Sale.vue";

export const routes = [
    {
        name: "DashBoard",
        path: "/",
        component: DashBoard,
        meta: { requireAuth: true },
        children: [
            {
                name: "Home",
                path: "",
                component: Home,
            },
            {
                name: "About",
                path: "about",
                component: About,
            },
            {
                name: "Sale",
                path: "sale",
                component: Sale,
            },
            {
                name: "Contact",
                path: "contact",
                component: Contact,
            },
            ...departmentRoutes,
        ],
    },
    {
        name: "Login",
        path: "/login",
        component: Login,
    },
    //...errorRoutes,
    // {
    //     path: "*",
    //     component: Page404,
    // },
];

const router = createRouter({
    routes: routes,
    history: createWebHistory(""),
    // history: createWebHistory("/laravel8ssup/public/"),
    linkExactActiveClass: "active",
});

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requireAuth)) {
        const token = localStorage.getItem("token");
        if (!token) {
            next({ path: "/login" });
        } else {
            next();
        }
    } else {
        next();
    }

    // else next();
    // if (to.name !== "Login" && !isAuthenticated) next({ name: "Login" });
    // else next();
});

export default router;
