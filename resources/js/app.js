import { createApp } from "vue";
import Vue from "vue";

require("./bootstrap");

import App from "./App.vue";
import Router from "./routers/index"; //index.js

import "@hennge/vue3-pagination/dist/vue3-pagination.css";

const app = createApp(App);

app.use(Router);
app.mount("#app");
