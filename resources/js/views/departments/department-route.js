import { h, resolveComponent } from "vue";

import DepartmentsIndex from "./DepartmentsIndex.vue";
import DepartmentCreate from "./DepartmentCreate.vue";
import DepartmentUpdate from "./DepartmentUpdate.vue";

const departmentRoutes = [
    {
        name: "Departments",
        path: "departments",
        component: {
            render: () => h(resolveComponent("router-view")),
        },
        children: [
            {
                name: "DepartmentsIndex",
                path: "",
                component: DepartmentsIndex,
            },
            {
                name: "DepartmentCreate",
                path: "create",
                component: DepartmentCreate,
            },
            {
                name: "DepartmentUpdate",
                path: "update/:id",
                component: DepartmentUpdate,
            },
        ],
    },
];

export default departmentRoutes;
