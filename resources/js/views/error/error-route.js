import { h, resolveComponent } from "vue";

import page404 from "./page404";

const errorRoutes = [
    {
        path: "*",
        component: page404,
    },
];

export default errorRoutes;
