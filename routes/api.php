<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CompanyController;
use App\Http\Controllers\API\DepartmentsController;
use App\Http\Controllers\API\OfficersController;
use App\Http\Controllers\API\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/company', [CompanyController::class, 'index']);

Route::get('/company/{email}', [CompanyController::class, 'contact']);



Route::apiResource('/product', ProductController::class);

Route::apiResource('/departments', DepartmentsController::class);

// Seach
Route::get('/search/departments', [DepartmentsController::class, 'search'])->middleware('auth:sanctum');

Route::apiResource('/officers', OfficersController::class);


//auth
Route::post('/auth/register', [AuthController::class, 'register']);
Route::post('/auth/login', [AuthController::class, 'login']);
Route::post('/auth/logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');
