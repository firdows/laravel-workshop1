<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Department;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isEmpty;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pageSize = request()->query('page_size', 3);
        $model = Department::paginate($pageSize);
        return response()->json($model, 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request);
        // exit();
        // $model = Department::create($request);
        // // $model->load($request);
        // // $model->save();
        // return ['success' => 1];
        // return  $request;

        // $model = new Department();
        // $model->title = $request->title;
        // $model->save();

        $request->title = 'test';

        $model = Department::create($request->all());
        //return ['success' => 1];

        return response()->json([
            'message' => 'เพิ่มข้อมูลสำเร็จ',
            'data' => $model
        ], 201);

        print_r($request);
        exit();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $model = Department::find($id);
        return response()->json([
            'message' => 'เพิ่มข้อมูลสำเร็จ',
            'data' => $model
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // if ($id != $request->id) {
        //     return response()->json([
        //         'errors' => [
        //             'status_code' => 400,
        //             'message' => 'รหัสไม่ตรงกัน'
        //         ]
        //     ]);
        // }

        $model = Department::find($id);
        $model->title = $request->title;
        $model->save();

        // $model->update($request->all());

        return response()->json([
            'message' => 'เพิ่มข้อมูลสำเร็จ',
            'data' => $model
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function search(Request $request)
    {
        //
        $title = $request->input('title');
        // dd($request);
        $model = Department::where('title', 'like', "%{$title}%")->get();
        if (!$model->isEmpty()) {
            return response()->json([
                'data' => $model,
                'user' => $request->user(),
            ], 200);
        } else {
            return response()->json([
                'message' => 'ไม่พบข้อมูล',
            ], 404);
        }
    }
}
