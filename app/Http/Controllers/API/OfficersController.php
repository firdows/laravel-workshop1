<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Officer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use function PHPUnit\Framework\isEmpty;

class OfficersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pageSize = request()->query('page_size', 3);



        /** One to Many */
        // $model = Department::with(['officers' => function ($query) {
        //     $query->orderBy('salary', 'desc');
        // }])->get();


        /** One to One */
        // $model = Officer::with('department', 'user')
        //     ->get();


        /** One to One  Multi Level*/
        $model = User::with(['officer', 'officer.department'])
            ->get();


        // dd($model);

        return response()->json($model, 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $of = new Officer();

            if ($request->has('picture')) {
                $base64_image = $request->picture;
                
                @list($type, $file_data) = explode(';', $base64_image);
                @list(, $file_data) = explode(',', $file_data);

                $new_filename = uniqid() . '.png';

                if ($file_data != "") {
                    Storage::disk('public')->put('uploads/' . $new_filename, base64_decode($file_data));
                }
                $of->firstname = $request->firstname;
                $of->lastname = $request->lastname;
                $of->dob = $request->dob;
                $of->salary = $request->salary;
                $of->user_id = $request->user_id;
                $of->department_id = $request->department_id;
                $of->picture = $new_filename;
            } else {
                $of->firstname = $request->firstname;
                $of->lastname = $request->lastname;
                $of->dob = $request->dob;
                $of->salary = $request->salary;
                $of->user_id = $request->user_id;
                $of->department_id = $request->department_id;
            }

            $of->save();
            DB::commit();

            return response()->json([
                'message' => 'เพิ่มข้อมูลพนักงานเรียบร้อย'
            ], 201);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'เกิดข้อผิดพลาดในการเพิ่มข้อมูล',
                'system message' => $th->getMessage()
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        return $request->salary;
        $model = Officer::find($id);
        $model->salary = $request->salary;
        $model->save();
        //$model->update($request->all());

        return response()->json([
            'message' => 'เพิ่มข้อมูลสำเร็จ',
            'data' => $model
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function search(Request $request)
    {
        //
        $title = $request->input('title');
        // dd($request);
        $model = Department::where('title', 'like', "%{$title}%")->get();
        if (!$model->isEmpty()) {
            return response()->json([
                'data' => $model
            ], 200);
        } else {
            return response()->json([
                'message' => 'ไม่พบข้อมูล',
            ], 404);
        }
    }
}
