<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class DefaultAcceptJson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // if (!$request->headers->has('Accept')) {
        $request->headers->set('Accept', 'application/json', true);
        // }
        // print_r($request->headers);
        // exit();

        return $next($request);
    }
}
