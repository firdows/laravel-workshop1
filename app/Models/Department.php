<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $table = "departments";

    //public $incrementing = false;
    protected $fillable = ['title'];

    // public function officer()
    // {
    //     return $this->belongsTo(Officer::class);
    // }

    public function officers()
    {
        return $this->hasMany(Officer::class);
    }
}
