<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Officer extends Model
{
    use HasFactory;

    protected $table = "officers";

    //public $incrementing = false;
    // protected $fillable = ['title'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    protected $appends = ['fullname', 'age', 'picture_url'];

    public function getFullnameAttribute()
    {
        return $this->firstname . " " . $this->lastname;
    }

    public function getAgeAttribute()
    {
        return now()->diffInYears($this->dob);
    }

    public function getPictureUrlAttribute()
    {
        return asset('storage/uploads') . "/" . $this->picture;
    }

    public function setSalaryAttribute()
    {
        $this->attributes['salary'] = 5555;
    }
}
